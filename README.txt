CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Configuration
 
INTRODUCTION
------------

 This modules enables feature of user registration and login with mobile number.
 Instead of username, user can login to the system using mobile number.
 
INSTALLATION
------------

 1. Copy the mobile_registration folder to your sites/all/modules or
   sites/{YOURSITENAME}/modules folder.

 2. Go to Administer -> Modules and enable Mobile Registration module.

CONFIGURATION
-------------

 1. Go to Administration > Configuration > People > Registration & Cancellation.
 2. Enable checkbox of "Allow users login with mobile number or username".
